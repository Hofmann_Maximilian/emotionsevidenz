#include "dempster.h"

/*
name:
	getPossibilities
input :
	int speed  = category of speed  (0=slow, 1=normal, 2=fast  )
	int pitch  = category of pitch  (0=low , 1=normal, 2=high  )
	int volume = category of volume (0=weak, 1=normal, 2=strong)
output :
	basicMeasure* = The combined evidence of the emotions
description :
	This function assigns suitable evidence and combines them.
*/
basicMeasure* getPossibilities(int speed, int pitch, int volume);

/*
name:
	getMostPlausibleEmotion
input :
	basicMeasure* possibilies = The combined evidence of the emotions
output :
	char* = string, containing the most plaosible emotion(s)
description :
	This function evaluates the possibilites given to it, and returns the most likely
*/
char* getMostPlausibleEmotion(basicMeasure* possibilities);
