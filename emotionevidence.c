#include "emotionevidence.h"
#include "asprintf.h"
#include <stdio.h>
#include <stdlib.h>

#define NUMOFEMOTIONS 6

basicMeasure getSpeedEvidence(int speed) {

	basicMeasure m;
	if (speed == 0) { //slow
		//                                  A, E, F, T, U, W
		const int entry1[NUMOFEMOTIONS] = { 0, 1, 0, 0, 0, 0 };
		const int entry2[NUMOFEMOTIONS] = { 0, 0, 1, 0, 0, 0 };

		set* p1 = createAlternatives((int*)entry1, NUMOFEMOTIONS);
		set* p2 = createAlternatives((int*)entry2, NUMOFEMOTIONS);

		createBasicMeasure(&m, NUMOFEMOTIONS);
		addMeasureEntry(&m, *p1, 0.7f); //Indicates disgust(Ekel)
		addMeasureEntry(&m, *p2, 0.1f); //Sometimes happyness(Freude)

		return m;
	}
	else if (speed == 2) { //fast
		//                                 A, E, F, T, U, W
		const int entry[NUMOFEMOTIONS] = { 1, 0, 1, 0, 1, 1 };

		set* p = createAlternatives((int*)entry, NUMOFEMOTIONS);

		createBasicMeasure(&m, NUMOFEMOTIONS);
		addMeasureEntry(&m, *p, 0.8f); //Indicates fear(Angst), happyness(Freude), surprise(Ueberrascht) or anger(Wut)

		return m;
	}
	else { //Normal or wrong values
		const int entry[NUMOFEMOTIONS] = { 1, 1, 1, 1, 1, 1 };
		set* p = createAlternatives((int*)entry, NUMOFEMOTIONS);
		createBasicMeasure(&m, NUMOFEMOTIONS);
		addMeasureEntry(&m, *p, 1.0f); //No Statement
		return m;
	}
}

basicMeasure getPitchEvidence(int pitch) {

	basicMeasure m;
	if (pitch == 0) { //low
		//                                  A, E, F, T, U, W
		const int entry1[NUMOFEMOTIONS] = { 0, 1, 0, 0, 0, 0 };
		const int entry2[NUMOFEMOTIONS] = { 0, 0, 0, 1, 0, 0 };

		set* p1 = createAlternatives((int*)entry1, NUMOFEMOTIONS);
		set* p2 = createAlternatives((int*)entry2, NUMOFEMOTIONS);

		createBasicMeasure(&m, NUMOFEMOTIONS);
		addMeasureEntry(&m, *p1, 0.7f); //Indicates disgust(Ekel)
		addMeasureEntry(&m, *p2, 0.1f); //Sometimes sadness(Trauer)

		return m;
	}
	else if (pitch == 2) { //high
		//                                 A, E, F, T, U, W
		const int entry[NUMOFEMOTIONS] = { 1, 0, 1, 0, 1, 1 };

		set* p = createAlternatives((int*)entry, NUMOFEMOTIONS);

		createBasicMeasure(&m, NUMOFEMOTIONS);
		addMeasureEntry(&m, *p, 0.8f); //Indicates fear(Angst), happyness(Freude), surprise(Ueberrascht) or anger(Wut)

		return m;
	}
	else { //Normal or wrong values
		const int entry[NUMOFEMOTIONS] = { 1, 1, 1, 1, 1, 1 };
		set* p = createAlternatives((int*)entry, NUMOFEMOTIONS);
		createBasicMeasure(&m, NUMOFEMOTIONS);
		addMeasureEntry(&m, *p, 1.0f); //No Statement
		return m;
	}
}

basicMeasure getVolumeEvidence(int volume) {

	basicMeasure m;
	if (volume == 0) { //weak
		//                                 A, E, F, T, U, W
		const int entry[NUMOFEMOTIONS] = { 0, 1, 0, 1, 0, 0 };

		set* p = createAlternatives((int*)entry, NUMOFEMOTIONS);

		createBasicMeasure(&m, NUMOFEMOTIONS);
		addMeasureEntry(&m, *p, 0.8f); //Indicates disgust(Ekel) or sadness(Trauer)

		return m;
	}
	else if (volume == 2) { //strong
		//                                 A, E, F, T, U, W
		const int entry[NUMOFEMOTIONS] = { 0, 0, 1, 0, 1, 1 };

		set* p = createAlternatives((int*)entry, NUMOFEMOTIONS);

		createBasicMeasure(&m, NUMOFEMOTIONS);
		addMeasureEntry(&m, *p, 0.8f); //Indicstes happyness(Freude), surprise(Ueberrascht) or anger(Wut)

		return m;
	}
	else { //Normal or wrong value
		const int entry[NUMOFEMOTIONS] = { 1, 1, 1, 1, 1, 1 };
		set* p = createAlternatives((int*)entry, NUMOFEMOTIONS);
		createBasicMeasure(&m, NUMOFEMOTIONS);
		addMeasureEntry(&m, *p, 1.0f); //No statement
		return m;
	}
}

basicMeasure* getPossibilities(int speed, int pitch, int volume) {
	if (speed == 1 && pitch == 1 && volume == 1)
		return NULL; //No statement possible
	
	basicMeasure m1, m2, m3, *temp;

	m1 = getSpeedEvidence(speed);
	m2 = getPitchEvidence(pitch);
	m3 = getVolumeEvidence(volume);

	temp = getAccumulatedMeasure(&m1, &m2);
	return getAccumulatedMeasure(temp, &m3);
}

char* getMostPlausibleEmotion(basicMeasure* possibilities) {
	char* mostPlausible;
	asprintf(&mostPlausible, "");

	if (possibilities == NULL) { //All attributes about the speechsample is normal => No statement possible
		asprintf(&mostPlausible, "Keine Emotion ueberwiegt");
		return mostPlausible;
	}

	//Determine the most plausible emotion(s)
	int i, j, max=0;
	int equal[NUMOFEMOTIONS] = { 0,-1,-1,-1,-1,-1 };
	for (i = 1; i < NUMOFEMOTIONS; i++) {
		if (plausibility(possibilities, i) > plausibility(possibilities, max)) {
			max = i;
			equal[0] = i;
			equal[1] = -1;
			equal[2] = -1;
			equal[3] = -1;
			equal[4] = -1;
		}
		else if (plausibility(possibilities, i) == plausibility(possibilities, max)) {
			j = 0;
			while (equal[j] != -1)
				j++;
			equal[j] = i;
		}
	}

	//Create the output message
	i = 0;
	if (equal[i] == 0) {
		asprintf(&mostPlausible, "Angst");
		i++;
	}
	if (equal[i] == 1) {
		if(i==0)
			asprintf(&mostPlausible, "Ekel");
		else
			asprintf(&mostPlausible, "%s, Ekel", mostPlausible);
		i++;
	}
	if (equal[i] == 2) {
		if (i == 0)
			asprintf(&mostPlausible, "Freude");
		else
			asprintf(&mostPlausible, "%s, Freude", mostPlausible);
		i++;
	}
	if (equal[i] == 3) {
		if (i == 0)
			asprintf(&mostPlausible, "Traur");
		else
			asprintf(&mostPlausible, "%s, Trauer", mostPlausible);
		i++;
	}
	if (equal[i] == 4) {
		if (i == 0)
			asprintf(&mostPlausible, "Ueberrraschung");
		else
			asprintf(&mostPlausible, "%s, Ueberraschung", mostPlausible);
		i++;
	}
	if (equal[i] == 5) {
		if (i == 0)
			asprintf(&mostPlausible, "Wut");
		else
			asprintf(&mostPlausible, "%s, Wut", mostPlausible);
		i++;
	}


	if (i == 0) {
		//Should not occure
		asprintf(&mostPlausible, "Es ist ein Fehler unterlaufen");
	}
	else {
		//End the string by adding the plausibiliy value
		asprintf(&mostPlausible, "%s (%3.2f%%)", mostPlausible, (((double)plausibility(possibilities, max)) * 100));
	}

	return mostPlausible;
}