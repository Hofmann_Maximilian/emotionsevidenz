# Emotionsevidenz

Drei Sequenzen von Messwerten zu je einer Audioaufnahme liegen vor. 
Die Auswertung erfolgt in einem Raster von ca. 3 Sekunden und ermittelt �ber diese die genannten Werte. 
In jeder Sequenz wird eine Menge von Emotionen in kurzer Folge von einer Person sprachlich dargestellt. 

Das Programm soll die jeweilige Datei einlesen und die Sequenz von Emotionen herausfinden, die dargestellt wurden. 
Dazu soll das auf dem Aufgabenblatt formulierte Hintergrundwissen in Form von Evidenzen verwendet werden.
