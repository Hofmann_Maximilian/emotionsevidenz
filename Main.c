#include "fileread.h"
#include "emotionevidence.h"


void main(int argc, char** argv) {
	//Get the filepath
	char* filepath;
	if (argc >= 2)
		filepath = (char*)argv[1];
	else
		filepath = "C:\\Users\\user\\Desktop\\DHBW\\Semester 5\\KI\\E_B03\\RealProject\\sequences\\E_B03_Sequenz_2.csv";

	//Read and compute the data
	data data = analysis(filepath);
	if (data.sqc_length == -1) {
		printf("Filepath ist nicht korrekt oder gesperrt. Ueberpruefen und neu probieren");
		return 1;
	}

	printf("Wahrscheinlichste Emotion: \n");

	//Compute Emotions and print them out
	int i, j;
	for (i = 0; i < data.sqc_length; i++) {
		basicMeasure* possibilities = getPossibilities(data.speed[i], data.pitch[i], data.volume[i]);

		char* mostPlausible = getMostPlausibleEmotion(possibilities);

		printf("\tSequenz %3d: %s\n", i + 1, mostPlausible);
	}

	printf("Alle Angaben ohne Gewaehr");
	
	return 0;
}