#include "fileread.h"

/*
name:
	init_struct
input:
	/
output:
	data = struct data with initialized fields
description:
	This function initializes the struct data to avoid memory artifacts and to make it 
	possible to use a while function to proof the end of the sequence.
comment:
	This function is not really necessary, because the functions are working with for-loops.
	But it is nicer to work with fixed preinitialized values than with automatic initiated array fields.
*/
data init_struct() {
	data stru;
	for (int i = 0; i < MAX_SEQUENCE_LENGTH; i++) {
		stru.speed[i] = -1; //initializing with -1 and not 0 to avoid problems if a real value is zero
		stru.pitch[i] = -1;
		stru.volume[i] = -1;
	}
	stru.sqc_length = 0; //can be 0; will be overwritten in any case
	return stru;
}

/*
name:
	getDataFromFile
input:
	char *filepath	=	pointer to the path string of the file
output:
	data = struct data with relevant data extracted from the file
description:
	This Function takes the csv-file of the sequences, parses the data and saves them to a struct	
*/
data getDataFromFile(char *filepath) {
	data fdata = init_struct();
	FILE *fptr;	
	if (fopen_s(&fptr, filepath, "r") != 0) {
		fdata.sqc_length = -1;
		return fdata;
	}
	char *speed;
	char *pitch;
	char *volume;
	char *context;
	int index = 0;

	char rowbuf[MRL];
	fgets(rowbuf, MRL, fptr); //discard csv header
	
	// while loop to read the file line by line and stops if the sequence is longer than the set value
	while (fgets(rowbuf, MRL, fptr) && index < MAX_SEQUENCE_LENGTH) {
		//parsing the current line of the file by using the ";" as delimiter
		speed = strtok_s(rowbuf, ";", &context);		
		pitch = strtok_s(NULL, ";", &context);
		volume = strtok_s(NULL, ";", &context);
		fdata.speed[index] = atoi(speed);
		fdata.pitch[index] = atoi(pitch);
		fdata.volume[index] = atoi(volume);
		index++;
	}

	fclose(fptr);
	fdata.sqc_length = index;
	return fdata;
};

/*
name:
	mean
input :
	int *sqc = pointer to the start value of the integer array of the attribute sequence
	int sqc_length = length of the sequence
output :
	float = computed mean value
description :
	This function computes the mean of the given attribute sequence.
*/
float mean(int *sqc, int sqc_length) {
	int sum = 0;
	int *help;
	help = sqc;
	for (int i = 0; i < sqc_length; i++) {
		sum = sum + *help;
		help++;
	}
	float m = ((float)sum) / sqc_length;
	return m;
}

/*
name:
	stdDeviation
input :
	int *sqc		= pointer to the start value of the integer array of the attribute sequence
	int sqc_length	= length of the sequence
	float mean		= mean of the given attribute sequence
output :
	float = computed value of the standard deviation
description :
	This function computes the standard deviation of the given attribute sequence.
*/
float stdDeviation(int *sqc, int sqc_length, float mean) {
	float quaddif = 0;
	float sum = 0;
	int *help;
	help = sqc;
	for (int i = 0; i < sqc_length; i++) {
		quaddif = (((float)*help - mean)*((float)*help - mean));
		sum = sum + quaddif;
		help++;
	}
	float rel = sum / ((float)sqc_length - 1);
	float stddev = sqrt(rel);
	float corr = 0.9; //correcting factor to decrease the size of the standard deviation and in the process that of the 'normal'-Interval
	return corr*stddev;
}

/*
name:
	gaussBorders
input :
	float mean		= mean of the attribute sequence
	float sigma 	= standard deviation of the attribute sequence
output :
	borders = struct borders which contains the values to divide the values into the enum categorys
description :
	This function computes the borders of the classification using the originally 68,27% Interval of the Gauss distribution.
*/
borders gaussBorders(float mean, float sigma) {
	borders result;
	result.border1 = mean - sigma;
	result.border2 = mean + sigma;
	return result;
}

/*
name:
	singleSequenceGauss
input:
	int *sqc		= pointer to the start value of the integer array of the attribute sequence
	int sqc_length	= length of the sequence
output:
	borders = struct borders which contains the values to divide the values into the enum categorys
description:
	This Function uses the extracted sequence data of an attribute to compute the borders between the enum categorys.
	It uses an approximation of the gauss distribution.
*/
borders singleSequenceGauss(int *sqc, int sqc_length) {
	float m = mean(sqc, sqc_length);
	float s = stdDeviation(sqc, sqc_length, m);
	borders sqc_borders = gaussBorders(m, s);
	return sqc_borders;
}

/*
name:
	classification
input:
	data sequences	= struct with the integer data of the sequence
	borders speed	= computed borders between the categorys low/normal/high of the speed attribute values
	borders pitch	= computed borders between the categorys low/normal/high of the pitch attribute values
	borders volume	= computed borders between the categorys low/normal/high of the volume attribute values
output:
	data = struct data with translated values from int to the defined category
description:
	This Function uses the extracted origin data and the computed borders for each category and attribute to 
	translate the int values to the enum category values LOW, NORM, HIGH
*/
data classification(data sequences, borders speed, borders pitch, borders volume) {
	data translate = init_struct();
	translate.sqc_length = sequences.sqc_length;
	int *help_speed;
	int *help_pitch;
	int *help_volume;
	help_speed = &sequences.speed[0];
	help_pitch = &sequences.pitch[0];
	help_volume = &sequences.volume[0];

	//for loop uses the given borders to transform every interger value of the attributes into the corresponding category
	for (int i = 0; i < sequences.sqc_length; i++) {
		if (*help_speed <= speed.border1) {
			translate.speed[i] = LOW;
		}
		else if (*help_speed > speed.border1 && *help_speed < speed.border2) {
			translate.speed[i] = NORM;
		}
		else if (*help_speed >= speed.border2) {
			translate.speed[i] = HIGH;
		}

		if (*help_pitch <= pitch.border1) {
			translate.pitch[i] = LOW;
		}
		else if (*help_pitch > pitch.border1 && *help_pitch < pitch.border2) {
			translate.pitch[i] = NORM;
		}
		else if (*help_pitch >= pitch.border2) {
			translate.pitch[i] = HIGH;
		}

		if (*help_volume <= volume.border1) {
			translate.volume[i] = LOW;
		}
		else if (*help_volume > volume.border1 && *help_volume < volume.border2) {
			translate.volume[i] = NORM;
		}
		else if (*help_volume >= volume.border2) {
			translate.volume[i] = HIGH;
		}

		help_speed++;
		help_pitch++;
		help_volume++;
	}
	
	return translate;
}

/*
name:
	analysis
input:
	char *filepath	=	pointer to the path string of the file
output:
	data = struct data with classification data computed from the file
description:
	This function is a wrapper for the full functionality from reading and parsing the csv-file 
	to classification of the extracted data.
*/
data analysis(char *filepath) {
	data sequences = getDataFromFile(filepath);

	if (sequences.sqc_length == -1) {
		return sequences;
	}

	int *speed_ptr;
	speed_ptr = &sequences.speed[0];
	borders speed_bor = singleSequenceGauss(speed_ptr, sequences.sqc_length);

	int *pitch_ptr;
	pitch_ptr = &sequences.pitch[0];
	borders pitch_bor = singleSequenceGauss(pitch_ptr, sequences.sqc_length);

	int *volume_ptr;
	volume_ptr = &sequences.volume[0];
	borders volume_bor = singleSequenceGauss(volume_ptr, sequences.sqc_length);
	
	data result = classification(sequences, speed_bor, pitch_bor, volume_bor);
	
	return result;
}



