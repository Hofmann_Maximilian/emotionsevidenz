#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#define MAX_SEQUENCE_LENGTH 100 //maximal length of the sequence which will be extracted from the file
#define MRL 50 //maximal row length; determines the size of the buffer for the function that reads the file line by line

typedef struct {
	int speed[MAX_SEQUENCE_LENGTH]; //contains the values of the speed attribute
    int pitch[MAX_SEQUENCE_LENGTH]; //contains the values of the pitch attribute
    int volume[MAX_SEQUENCE_LENGTH]; //contains the values of the volume attribute
	int sqc_length; //length of the given sequence
} data;

typedef struct {
	float border1; //border between the LOW and the NORMAL category
	float border2; //border between the NORMAL and the HIGH category
} borders;

enum catgeories {
    LOW, NORM, HIGH
};

/*
name:
	analysis
input:
	char *filepath	=	pointer to the path string of the file
output:
	data = struct data with classification data computed from the file
description:
	This function uses on the adress the path string to read the file, parses the data and saves it to a struct.
	After that it performs a classification which transforms all integer values of each attribute of the sequence
	to one of the three categorys (LOW, NORMAL, HIGH) according to the relative amount of each value.
*/
data analysis(char *filepath);
